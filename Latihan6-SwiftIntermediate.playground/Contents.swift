import UIKit

// IF Statements
let temperature = 24

if temperature <= 24 {
    print("====> Dingin banget")
}

// if-else statements
let temperatureAC = 22
if temperatureAC >= 30 {
    print("====> Udara panas")
} else {
    print("====> Udara Dingin")
}


// if-else boolean value
let temperatureRuangan = 45

if temperatureRuangan >= 35 && temperatureRuangan <= 40 {
    print("====> Udara Palembang panas")
} else if temperatureRuangan == 22 {
    print("====> Cikarang panas")
} else {
    print("====> Warga surabaya sudah biasa")
}


// switch statements

let krlDeparture = 6

switch krlDeparture {
case 5:
    print("KRL masih kosong")
case 6:
    print("KRL mulai chaos")
case 7:
    print("Orang pada mulai WAR")
case 8:
    print("Resign aja lah")
default:
    print("Pasti pengangguran")
}


// Treanry Operator

var flashSale: Int
let a = 1500
let b = 4

if a > b {
    flashSale = a
} else {
    flashSale = b
}


// rewrite
flashSale = a > b ? a : b


// String Interpolation
let  name = "John Wick"
let age = 50

print("====> \(name) umurnya \(age)")


// Functions

func displayPi() {
    print("3.242592")
}

displayPi()


// Function dengan parameter
func triple(value: Int){
    let result = value * 3
    print("====> if you multiply \(value) by 3, you'll get \(result)")
}

//Function dengan multiple parameter
func multiply(firstNumber: Int, secondNumber: Int){
    let result = firstNumber * secondNumber
    print("====> The result is \(result)")
}

let hasil: () =  multiply(firstNumber: 10, secondNumber: 5)
print(hasil)


// Function ada return value
func multiplyBy2(fourthNumber: Int, fifthNumber: Int) -> Int {
    let result = fourthNumber + fifthNumber
    print("====> The result is \(result)")
    
    return result
}

let myResult = multiplyBy2(fourthNumber: 10, fifthNumber: 5)
print("====> 1- ditambah 5 sama dengan \(myResult)")


// Function yang ada argument labels
func sayHello(to person: String, and anotherPerson: String){
    print("Hwllo \(person) and \(anotherPerson)")
}

sayHello(to: "John", and: "Lisa")

// omitting labels
func add(_ firstNumber: Int, to secondNumber: Int = 0) -> Int {
    return firstNumber + secondNumber
}

add(20, to: 11)

// Struct : moffice
struct Shirt {
    var size: String
    var color: String
}

let myShirt = Shirt(size: "XL", color: "Black") //instances
let yourShirt = Shirt(size: "M", color: "Blue")


// Dari API biasanya dapat data JSON berupa Dictionary (key value)
var myDict: [String:Int] = [
    "a": 1,
    "b": 2,
    "c": 3
]

// menghapus item dari data dictionary
myDict.removeValue(forKey: "c")

// Mengakses itemd ari dictionary
let item = myDict["b"]

// mengupdate item dari dictionary
myDict["a"] = 5

// looping item didalam dictionary
for (key, value) in myDict {
    print("====> Isi dictionary saya adalah \(key): \(value)")
}

// Mendapatkan semua value dari dictionary
let values = Array(myDict.keys)
print(values)
