//
//  EmojiDetailView.swift
//  Latihan3-ListSwiftUI
//
//  Created by Macbook Pro on 24/03/23.
//

import SwiftUI

struct EmojiDetailView: View {
    
    let emojiDetails: EmojiModel
    
    var body: some View {
        VStack {
            Text(emojiDetails.emoji)
                .font(.system(size: 120))
                .padding(.bottom)
            
            Text(emojiDetails.name)
                .font(.system(.largeTitle,
                              design: .rounded))
                .padding(.bottom)
            
            Text(emojiDetails.description)
                .font(.system(size: 18, design: .rounded))
                .lineSpacing(4)
                .multilineTextAlignment(.center)
            
            
            Spacer()
            
        } // VSTACK
        . padding()
    }
}

struct EmojiDetailView_Previews: PreviewProvider {
    static var previews: some View {
        EmojiDetailView(emojiDetails: EmojiModel(emoji: "🤪", name: "Gila", description: "Orang gila adalah orang yang sudah tidak memiliki akal untuk berpikir, gila terjadi karena stress atau faktor tertentu, jika melihat orang gila berlarilah dan pergi ke rs. jiwa untuk meminta pertolongan agar orang gila tersebit segera dirawat."))
    }
}
