//
//  EmojiViewController.swift
//  Latihan3-TableViewUIKit
//
//  Created by Macbook Pro on 24/03/23.
//

import UIKit

class EmojiViewController: UIViewController {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emojiLabel: UILabel!
    
    var emoji: EmojiModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        emojiLabel.text = emoji.emoji
        nameLabel.text = emoji.name
        descriptionLabel.text = emoji.description
    }
}

extension UIViewController {
    func showEmojiViewController(emoji: EmojiModel){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "EmojiViewController") as! EmojiViewController
        viewController.emoji = emoji
        
        navigationController?.pushViewController(viewController, animated: true)
    }
}
