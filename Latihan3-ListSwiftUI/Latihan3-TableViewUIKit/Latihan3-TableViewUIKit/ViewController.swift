//
//  ViewController.swift
//  Latihan3-TableViewUIKit
//
//  Created by Macbook Pro on 24/03/23.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    weak var searchController: UISearchController!
    
    var emojis: [EmojiModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        emojis = EmojiProvider.all()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        let sc = UISearchController()
        navigationItem.searchController = sc
        self.searchController = sc
        navigationItem.hidesSearchBarWhenScrolling = false
        sc.searchBar.placeholder = "Search emoji"
        sc.searchBar.delegate = self
    }


}

extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            emojis = EmojiProvider.all()
        }else{
            emojis = EmojiProvider.all()
                .filter({ emoji in
                    return emoji.name.lowercased().contains(searchText.lowercased()) || emoji.emoji.contains(searchText)
                })
        }
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        emojis = EmojiProvider.all()
        tableView.reloadData()
    }
}

//extension adalah fitur di swift yang kita bisa menambahkan fungsionalitas terhadap objek atau apapun
// variable tidak bisa stored, yang bisa hanya compited

//stored property
//var emoji: EmojiModel!

//compute property
//extension String {
//var zizi: String {
//    return "lucu"
//}
//}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let emoji = emojis[indexPath.row]
        showEmojiViewController(emoji: emoji)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emojis.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "emoji_cell", for: indexPath) as! EmojiViewCell
        
        let emoji = emojis[indexPath.row]
        cell.emojiLabel.text = emoji.emoji
        cell.titleLabel.text = emoji.name
        
        return cell
    }
}

