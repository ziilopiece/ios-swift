//
//  PrayGuideViewController.swift
//  PrayerTime
//
//  Created by Macbook Pro on 11/04/23.
//

import UIKit

class PrayGuideViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    var prayGuideList: [ItemPrayGuideModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        prayGuideList = ItemPrayGuide.all()
        pageControl.numberOfPages = prayGuideList.count
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = Int(scrollView.contentOffset.x
        / scrollView.frame.width)
        pageControl.currentPage = page
    }
    
    //Mark: - Actions
    @IBAction func pageControlValueChanged(_ sender: Any) {
        let page = pageControl.currentPage
        collectionView.scrollToItem(at: IndexPath( item: page, section: 0) , at: .centeredHorizontally, animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width
        return CGSize(width: width, height: 500)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return prayGuideList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "guide_cell", for: indexPath) as! PrayGuideViewCell
        
        let prayGuide = prayGuideList[indexPath.item]
        cell.guideImageView.image =  UIImage(named: prayGuide.imageGuide)
        cell.guideTitleLabel.text = prayGuide.titleGuide
        cell.guideDoaLabel.text = prayGuide.doaGuide
        cell.guideLatinLabel.text = prayGuide.latinGuide
        cell.guideSubtitleLabel.text = prayGuide.subtitleGuide
        
        cell.widthConstraint.constant = UIScreen.main.bounds.width
        
        return cell
    }
}
