//
//  CollectionViewCell.swift
//  PrayerTime
//
//  Created by Macbook Pro on 11/04/23.
//

import UIKit

class PrayGuideViewCell: UICollectionViewCell {
    
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var guideImageView: UIImageView!
    @IBOutlet weak var guideSubtitleLabel: UILabel!
    @IBOutlet weak var guideLatinLabel: UILabel!
    @IBOutlet weak var guideDoaLabel: UILabel!
    @IBOutlet weak var guideTitleLabel: UILabel!
}
