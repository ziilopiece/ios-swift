//
//  ApiService.swift
//  MealMonkey
//
//  Created by Macbook Pro on 31/03/23.
//

import Foundation
import UIKit
import Alamofire

class PrayProviders {
    static let shared: PrayProviders = PrayProviders()
    private init() {}
    
    let baseUrlString = "https://api.banghasan.com/sholat/format/json"
    let doaURL = "https://doa-doa-api-ahmadramadhan.fly.dev/api"
    
    func loadSchedulePray(kodeKota: String, date: String , completion: @escaping (Result<JadwalData, Error>) -> Void) {
        let pathUrlString = "/jadwal/kota/\(kodeKota)/tanggal/\(date)"
        let url = URL(string: "\(baseUrlString)\(pathUrlString)")!
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(error))
                    return
                }
                
                guard let data = data else {
                    let error = NSError(domain: "com.example.app", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data tidak tersedia"])
                    completion(.failure(error))
                    return
                }
                
                do {
                    let jadwalResponse = try JSONDecoder().decode(JadwalSholatResponse.self, from: data)
                    let jadwalList = jadwalResponse.jadwal.data
                    completion(.success(jadwalList))
                } catch {
                    completion(.failure(error))
                }
            }
        }
            task.resume()
    }
    
    func fetchDoa(completion: @escaping (Result<[Doa], Error>) -> Void) {
            AF.request(doaURL)
                .validate()
                .responseDecodable(of: [Doa].self) { response in
                    switch response.result {
                    case .success(let doaList):
                        completion(.success(doaList))
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
        }
}
            
