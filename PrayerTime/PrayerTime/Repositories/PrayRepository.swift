//
//  PrayRepository.swift
//  PrayerTime
//
//  Created by Macbook Pro on 08/04/23.
//

import Foundation
import CoreData
import UIKit

class PrayRepository {
    static let shared: PrayRepository = PrayRepository()
    private init() { }
    
    var context: NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.context
    }
    
    
}
