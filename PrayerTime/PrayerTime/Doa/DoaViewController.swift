//
//  DoaViewController.swift
//  PrayerTime
//
//  Created by Macbook Pro on 10/04/23.
//

import UIKit

class DoaViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var doaList: [Doa] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
        loadDoa()

    }
    
    func setup() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func loadDoa() {
            PrayProviders.shared.fetchDoa { [weak self] result in
                switch result {
                case .success(let doaList):
                    self?.doaList = doaList
                    self?.tableView.reloadData()
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    
}


extension DoaViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return doaList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "doa_cell", for: indexPath) as! DoaViewCell
        cell.doaLabel.text = doaList[indexPath.row].doa
        cell.delegate = self
        return cell
        
    }
}


extension DoaViewController: UITableViewDelegate {
    
}

extension DoaViewController: DoaViewCellDelegate {
    func doaViewCellShowButtonTapped(_ cell: DoaViewCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            let doa = doaList[indexPath.row]
            showDetailViewController(doa: doa)
        }
    }
}
