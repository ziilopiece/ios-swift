//
//  DetailDoaViewController.swift
//  PrayerTime
//
//  Created by Macbook Pro on 10/04/23.
//

import UIKit

class DetailDoaViewController: UIViewController {

    @IBOutlet weak var artiLabel: UILabel!
    @IBOutlet weak var latinLabel: UILabel!
    @IBOutlet weak var ayatLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    var doa: Doa!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        titleLabel.text = doa.doa
        ayatLabel.text = doa.ayat
        latinLabel.text = doa.latin
        artiLabel.text = doa.artinya
    }
    
    
    

}

extension UIViewController {
    func showDetailViewController(doa: Doa){
        let storyboard = UIStoryboard(name: "Doa", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "detail") as! DetailDoaViewController
        viewController.doa = doa
        navigationController?.pushViewController(viewController, animated: true)
        
    }
}
