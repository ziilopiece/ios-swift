//
//  DoaViewCell.swift
//  PrayerTime
//
//  Created by Macbook Pro on 10/04/23.
//

import UIKit

protocol DoaViewCellDelegate: AnyObject {
    func doaViewCellShowButtonTapped(_ cell: DoaViewCell)
}

class DoaViewCell: UITableViewCell {
    @IBOutlet weak var doaLabel: UILabel!
    
    weak var delegate: DoaViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func showButtonTapped(_ sender: Any) {
        delegate?.doaViewCellShowButtonTapped(self)
    }
}
