//
//  Schedule.swift
//  PrayerTime
//
//  Created by Macbook Pro on 07/04/23.
//

import Foundation

struct JadwalSholatResponse: Codable {
    let status: String
    let query: Query
    let jadwal: Jadwal
}

struct Query: Codable {
    let format: String
    let kota: String
    let tanggal: String
}

struct Jadwal: Codable {
    let status: String
    let data: JadwalData
}

struct JadwalData: Codable {
    let ashar: String
    let dhuha: String
    let dzuhur: String
    let imsak: String
    let isya: String
    let maghrib: String
    let subuh: String
    let tanggal: String
    let terbit: String
}


