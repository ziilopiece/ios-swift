//
//  DoaModel.swift
//  PrayerTime
//
//  Created by Macbook Pro on 10/04/23.
//

import Foundation

struct Doa: Codable {
    let id: String
    let doa: String
    let ayat: String
    let latin: String
    let artinya: String
}


