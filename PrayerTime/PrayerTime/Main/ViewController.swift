//
//  ViewController.swift
//  PrayerTime
//
//  Created by Macbook Pro on 02/04/23.
//

import UIKit

class ViewController: UITabBarController, UITabBarControllerDelegate {
    weak var middleButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
      
        tabBar.items?.forEach({ item in
            item.image = item.image?.withRenderingMode(.alwaysOriginal)
            item.selectedImage = item.selectedImage?.withRenderingMode(.alwaysOriginal)
        })
        tabBar.tintColor = UIColor(named: "Main")
    }
}

