//
//  HomeViewController.swift
//  PrayerTime
//
//  Created by Macbook Pro on 04/04/23.
//

import UIKit
import SafariServices

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    
    var jadwalPrayList: JadwalData?
    var date = Date()
    
    enum Section {
        case pray
    }
    
    var sections: [Section] = [.pray]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setup()
        loadJadwal()
    }
    
    func setup() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @IBAction func previousButtonTapped(_ sender: Any) {
        self.date = date.addingTimeInterval(-86400)
        loadJadwal(date: self.date)
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        self.date = date.addingTimeInterval(86400)
        loadJadwal(date: self.date)
    }
    
    func loadJadwal(date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "EEEE, d MMMM yyyy"
        dateLabel.text = dateFormatter.string(from: date)
        
        PrayProviders.shared.loadSchedulePray(kodeKota: "703", date: dateString) { [weak self] (result) in
            switch result {
            case .success(let jadwalData):
                self?.jadwalPrayList = jadwalData
                self?.tableView.reloadData()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func loadJadwal(){
        PrayProviders.shared.loadSchedulePray(kodeKota: "703", date: "2017-02-07") { [weak self] (result) in
            switch result {
            case .success(let jadwalData):
                self?.jadwalPrayList = jadwalData
                self?.tableView.reloadData()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

extension HomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let value = sections[section]
        switch value {
        case .pray:
            return 7
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let value = sections[indexPath.section]
        switch value {
        case .pray:
            let cell =  tableView.dequeueReusableCell(withIdentifier: "pray_cell", for: indexPath) as! PrayViewCell
            
            switch indexPath.row {
            case 0:
                cell.titleLabel.text = "Subuh"
                cell.timeLabel.text = jadwalPrayList?.subuh
            case 1:
                cell.titleLabel.text = "Dhuha"
                cell.timeLabel.text = jadwalPrayList?.subuh
            case 2:
                cell.titleLabel.text = "Dzuhur"
                cell.timeLabel.text = jadwalPrayList?.dzuhur
            case 3:
                cell.titleLabel.text = "Ashar"
                cell.timeLabel.text = jadwalPrayList?.ashar
            case 4:
                cell.titleLabel.text = "Maghrib"
                cell.timeLabel.text = jadwalPrayList?.maghrib
            case 5:
                cell.titleLabel.text = "Isya"
                cell.timeLabel.text = jadwalPrayList?.isya
            case 6:
                cell.titleLabel.text = "Ashar"
                cell.timeLabel.text = jadwalPrayList?.subuh
            case 7:
                cell.titleLabel.text = "Imsak"
                cell.timeLabel.text = jadwalPrayList?.imsak
            default:
                print("data tidak ada")
            } 
            return cell
        }
    }
}

extension HomeViewController: UITableViewDelegate {
}
