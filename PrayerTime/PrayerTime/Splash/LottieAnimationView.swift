////
////  LottieAnimation.swift
////  PrayerTime
////
////  Created by Macbook Pro on 12/04/23.
////
//
//import SwiftUI
//import Lottie
//
//struct LottieAnimation: UIViewRepresentable {
//    let lottieFile: String
//    
//    static var shouldAnimate = true
//    
//    let animationView = LottieAnimationView()
//    
//    func makeUIView(context: Context) -> some UIView {
//        let view = UIView(frame: .zero)
//        
//        animationView.animation = LottieAnimation.named(lottieFile)
//        animationView.contentMode = .scaleAspectFit
//        animationView.loopMode = .playOnce
//        
//        let animationDuration = animationView.animation?.duration ?? 0
//        let animationSpeed = CGFloat(animationDuration / 5)
//        
//        animationView.animationSpeed = animationSpeed
//        animationView.play()
//        
//        view.addSubview(animationView)
//        
//        animationView.translatesAutoresizingMaskIntoConstraints =  false
//        animationView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
//        animationView.widthAnchor.constraint(equalTo: view.heightAnchor).isActive = true
//        
//        return view
//    }
//    
//    func updateUIView(_ uiView: UIViewType, context: Context) {
//        
//    }
//}
//
//
