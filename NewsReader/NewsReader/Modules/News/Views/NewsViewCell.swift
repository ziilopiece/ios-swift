//
//  NewsViewCell.swift
//  NewsReader
//
//  Created by Macbook Pro on 28/03/23.
//

import UIKit

protocol NewsViewCellDelegate: AnyObject {
    func newsViewCellBookmarkButtonTapped(_ cell: NewsViewCell)
}

class NewsViewCell: UITableViewCell {

    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    var delegete: NewsViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func bookmarkButtonTapped(_ sender: Any) {
        delegete?.newsViewCellBookmarkButtonTapped(self)
    }
}
