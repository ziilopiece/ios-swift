//
//  TopNewsViewCell.swift
//  NewsReader
//
//  Created by Macbook Pro on 27/03/23.
//

import UIKit

class TopNewsViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageview: UIImageView!
}
