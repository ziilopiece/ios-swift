//
//  ReadingViewController.swift
//  NewsReader
//
//  Created by Macbook Pro on 29/03/23.
//

import UIKit

class ReadingViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var readinglist: [NewsData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
        loadReadingList()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.readingListAdded(_:)), name: .readingListAdded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.readingListAdded(_:)), name: .readingListDeleted, object: nil)
        
        
    }
    
    @objc func readingListAdded(_ sender: Any) {
        loadReadingList()
        tableView.reloadData()
    }
    
    func loadReadingList() {
        self.readinglist = ReadingRepository.shared.fetchNews()
    }
    
    func setup(){
        tableView.dataSource = self
        
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "NewsViewCell", bundle: nil), forCellReuseIdentifier: "news_cell")
    }
}

// MARK : - UITableViewDataSource
extension ReadingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return readinglist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "news_cell", for: indexPath) as! NewsViewCell
        
        let news = readinglist[indexPath.row]
        cell.titleLabel.text = news.title
        cell.subtitleLabel.text = "\(news.publishedDate ?? "") · \(news.section ?? "")"
        let imageUrl = news.mediaUrl ?? ""
        NewsProvider.shared.downloadImage(urlString: imageUrl) { image in
            cell.thumbImageView.image = image
        }
        cell.bookmarkButton.isHidden = true
        
        return cell
    }
}


//MARK: - UITableViewDelegate
extension ReadingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let action = UIContextualAction(style: .destructive, title: "Delete") { action, view, completion in
            
            let news = self.readinglist.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                ReadingRepository.shared.deleteNews(newsId: Int(news.newsId))
            }
       
  
            
            completion(true)
        }
        
        return UISwipeActionsConfiguration(actions: [action])
    }
}
