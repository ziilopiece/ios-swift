//
//  ReadingRepository.swift
//  NewsReader
//
//  Created by Macbook Pro on 29/03/23.
//

import Foundation
import CoreData
import UIKit

class ReadingRepository {
    static let shared: ReadingRepository = ReadingRepository()
    private init() { }
    
    var context: NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.context
    }
    
    func saveNews(_ news: News){
        let request = NewsData.fetchRequest()
        request.predicate = NSPredicate(format: "newsId = \(news.id)")
        
        let newsData: NewsData
        if let data = try? context.fetch(request).last{
            newsData = data
        } else {
            newsData = NewsData(context: context)
        }
        
        newsData.newsId = Int64(news.id)
        newsData.title = news.title
        newsData.section = news.section
        newsData.publishedDate = news.publishedDate
        newsData.url = news.url
        newsData.abstract = news.abstract
        newsData.mediaUrl = news.media.first?.metadata.last?.url
        
        try? context.save()
        NotificationCenter.default.post(name: .readingListAdded, object: nil)
    }
    
    func fetchNews() -> [NewsData] {
        let request = NewsData.fetchRequest()
        return (try? context.fetch(request)) ?? []
    }
    
    func deleteNews(newsId: Int) {
        let request = NewsData.fetchRequest()
        request.predicate = NSPredicate(format: "newsId = \(newsId)")
        
        if let data = try? context.fetch(request).first {
            context.delete(data)
            try? context.save()
            NotificationCenter.default.post(name: .readingListDeleted, object: nil)
        }
    }
    
    func isAddedToReadingList(newsId: Int) -> Bool {
        let request = NewsData.fetchRequest()
        request.predicate = NSPredicate(format: "newsId = \(newsId)")
        
        if (try? context.fetch(request).first) != nil {
            return true
        } else {
            return false
        }
    }
}
