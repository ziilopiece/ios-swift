//
//  NotificationExtensions.swift
//  NewsReader
//
//  Created by Macbook Pro on 29/03/23.
//

import Foundation

extension Notification.Name {
    static var readingListAdded = Notification.Name(rawValue: "kReadingListAdded")
    static var readingListDeleted = Notification.Name(rawValue: "kReadingListDeleted")
}
