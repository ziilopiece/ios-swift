import UIKit

let people = [
    "Jack Ryan",
    "John Wick",
    "Jason Bournce",
    "James Bond",
    "Tom Clancys",
    "Jack Ryan Jr",
    "James Greer",
    "Kathleen Ryan",
    "Scott Mitchell",
    "Sam Driscoll"
]

// Contoh Closures dengan parameter dab return type
let theRyans = people.filter({ (name: String) -> Bool in
    return name.contains("Ryan")
})

// Contoh Closures dimana return type nya bisa di omit (dihilangkan)
let theJacks = people.filter { name in
    return name.contains("Ryan")
}

// Shorthand Closures, $0, $1 sebagai pengganti argumen
let theJackPart2 = people.filter({ $0.contains("Jack") })
theJackPart2

// Trailing closures
let theJackPart3 = people.filter{ $0.contains("Jack") }
theJackPart3
