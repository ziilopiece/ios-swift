//
//  SignUpViewController.swift
//  MealMonkey
//
//  Created by Macbook Pro on 31/03/23.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var signUpButton: PrimaryButton!
    @IBOutlet weak var confirmPasswordTextField: InputTextField!
    @IBOutlet weak var passwordTextField: InputTextField!
    @IBOutlet weak var emailTextField: InputTextField!
    @IBOutlet weak var nameTextField: InputTextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let index = navigationController?.viewControllers.firstIndex(where: {$0 is LoginViewController}){
            navigationController?.viewControllers.remove(at: index)
        }
    }
    
//    MARK: - Button Handler
    @IBAction func signUpButtonTapped(_ sender: Any) {
        if validateInput(){
            signUp()
        }
    }
    
    
    func validateInput()->Bool{
        let showAlert : (String)-> Void = {
            (message) in
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default))
            self.present(alert, animated: true)
        }
        guard let name = nameTextField.text, name.count >= 3 else{
            showAlert("Minimum 3 character")
            return false
        }
       
        guard let email = emailTextField.text, email.isEmail else{
            showAlert("email is not valid")
            return false
        }
        
        guard let password = passwordTextField.text, password.count >= 4 else{
            showAlert("Minimum 4 character")
            return false
        }
        guard let confirmPassword = confirmPasswordTextField.text, confirmPassword == password else{
            showAlert("Confirm password dismatch")
            return false
        }
        
        return true
    }
    
//    MARK:- SignUp Function
    func signUp(){
        let name  = nameTextField.text ?? ""
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        let user = Users(name: name, password: password, email: email)
        showLoadingView(message: "Process sign up")
        ApiService.shared.singup(user: user) { [weak self] (error) in
            self?.hideLoadingView()
            if let error = error {
                
                print(error.localizedDescription)
            }else{
                self?.performSegue(withIdentifier: "showLogin", sender: nil)
            }
        }
        
        
    }//:Sign Up
}
