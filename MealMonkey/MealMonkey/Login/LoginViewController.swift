//
//  LoginViewController.swift
//  MealMonkey
//
//  Created by Macbook Pro on 30/03/23.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var passwordTextField: InputTextField!
    @IBOutlet weak var emailTextField: InputTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let index = navigationController?.viewControllers.firstIndex(where: {$0 is SignUpViewController}){
            navigationController?.viewControllers.remove(at: index)
        }
    }
    
    func validateInput()->Bool{
        let showAlert : (String)-> Void = {
            (message) in
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default))
            self.present(alert, animated: true)
        }
        guard let email = emailTextField.text, email.count > 0 else{
            showAlert("Please input email address")
            return false
            
        }
        guard let password = passwordTextField.text, password.count > 0 else{
            showAlert("Please input password")
            return false
            
        }
        return true
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        if validateInput() {
            login()
        }

    }
    
    func login(){
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        showLoadingView(message: "Please wait, login processed")
        
        ApiService.shared.login(email: email, password: password) { [weak self] (result) in
            switch result{
            case .success(let accessToken):
                self?.showLoadingView(message: "Load Profile")
                self?.getProfile(accessToken: accessToken.accessToken)
                UserDefaults.standard.saveAccessToken(accessToken)
            case .failure(let error):
                
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default))
                    self?.present(alert, animated: true)
             
                
                self?.hideLoadingView()
                print(error.localizedDescription)
            }

        }
    }
    
    func getProfile(accessToken:String){
        ApiService.shared.getProfile(accessToken: accessToken) { [weak self](result) in
            self?.hideLoadingView()
            switch result{
            case .success(let user ):
                print("\(user.id) \(user.name) \(accessToken)")
            case .failure(let error):
                print(error.localizedDescription)
            }
            self?.goToMain()
        }
    }
}
