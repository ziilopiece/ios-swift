//
//  Users.swift
//  MealMonkey
//
//  Created by Macbook Pro on 31/03/23.
//

import Foundation

struct Users : Codable {
    let id : Int
    let name: String
    let email: String
    let password: String
    let avatar: String
    let role: String
    
    enum CodingKeys:String, CodingKey{
        case id
        case name
        case email
        case password
        case avatar
        case role
        
    }
    
//    Object to Data
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.email, forKey: .email)
        try container.encode(self.password, forKey: .password)
        try container.encode(self.avatar, forKey: .avatar)
        try container.encode(self.role, forKey: .role)
        try container.encode(self.id, forKey: .id)
    }
//    Data To Object
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.email = try container.decode(String.self, forKey: .email)
        self.password = try container.decode(String.self, forKey: .password)
        self.avatar = try container.decode(String.self, forKey: .avatar)
        self.role = try container.decode(String.self, forKey: .role)
    }
    
    init(name:String, password:String,email:String){
        self.email = email
        self.name = name
        self.password = password
        
        self.id = 0
        self.role = "customer"
        self.avatar = "https://ui-avatars.com/api/?name=Wahyu-Mulyadi&color=FFFFFF&background=FF8045&size=640&bold=true"
    }
}
