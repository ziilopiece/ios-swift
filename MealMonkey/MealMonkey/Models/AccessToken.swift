//
//  AccessToken.swift
//  MealMonkey
//
//  Created by Macbook Pro on 31/03/23.
//

import Foundation


struct AccessToken: Codable{
    let accessToken: String
    let refreshToken: String
    
    enum CodingKeys: String, CodingKey{
        case accesToken = "access_token"
        case refreshToken = "refresh_token"
    }
    
    
//    Data to Object
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        accessToken = try container.decodeIfPresent(String.self, forKey: .accesToken) ?? ""
        refreshToken = try container.decodeIfPresent(String.self, forKey: .refreshToken) ?? ""
        
    }
    
    
    
//    objsect to data
    func encode(to encoder: Encoder) throws {
        var contariner =  encoder.container(keyedBy: CodingKeys.self)
        try contariner.encode(accessToken, forKey: .accesToken)
        try contariner.encode(refreshToken, forKey: .refreshToken)
    }
    
    
    
}
