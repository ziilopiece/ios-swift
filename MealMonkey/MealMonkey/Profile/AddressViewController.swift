//
//  AddressViewController.swift
//  MealMonkey
//
//  Created by Macbook Pro on 06/04/23.
//

import UIKit
import MapKit
class AddressViewController: UIViewController {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var completion:(CLLocation, String) -> Void = {
        _, _ in
    }
    var location: CLLocation?
    var address: String?
    let loactionManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.showsUserLocation = true
        let gestrure = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressHandler(_:)))
        gestrure.minimumPressDuration = 0.5
        gestrure.delaysTouchesBegan = true
//        gestrure.delegate
        
        mapView.addGestureRecognizer(gestrure)
        loactionManager.requestWhenInUseAuthorization()
        loactionManager.desiredAccuracy = kCLLocationAccuracyBest
        loactionManager.distanceFilter = kCLDistanceFilterNone
        loactionManager.startUpdatingLocation()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        zoomToLocation()
        getAddressFromCoordinate(mapView.userLocation.coordinate)
    }
    @objc func longPressHandler(_ sender: UILongPressGestureRecognizer){
        if sender.state != .ended{
            let location = sender.location( in: mapView)
            let coordinate = mapView.convert(location, toCoordinateFrom: mapView)
            getAddressFromCoordinate(coordinate)
        }
    }
    
    func getAddressFromCoordinate(_ coordinate: CLLocationCoordinate2D){
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)) { placemarks, error in
            if let placemark = placemarks?.first{
                self.location = placemark.location
                self.address = placemark.thoroughfare
                self.addressLabel.text = placemark.thoroughfare
            }
        }
    }
    
    func zoomToLocation(){
    
        if let location = mapView.userLocation.location{
            let viewRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 200, longitudinalMeters: 200)
            mapView.setRegion(viewRegion, animated: true)
            
        }
    }
    @IBAction func saveButtonTapped(_ sender: Any) {
        dismiss(animated: true){
            if let location = self.location, let address = self.address{
                self.completion(location,address)
            }
        }
        
    }
    
   

}


extension UIViewController{
    func showAddressViewController(completion: @escaping (CLLocation,String) -> Void){
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "address") as! AddressViewController
        viewController.completion = completion
        present(viewController, animated: true)
    }
}
