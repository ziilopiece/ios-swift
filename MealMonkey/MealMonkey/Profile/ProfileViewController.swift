//
//  ProfileViewController.swift
//  MealMonkey
//
//  Created by Macbook Pro on 06/04/23.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var addressTextField: InputTextField!
    @IBOutlet weak var emailTextField: InputTextField!
    @IBOutlet weak var nameTextField: InputTextField!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    var profileImage: UIImage?{
        didSet{
            profileImageView.image = profileImage
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let user = UserDefaults.standard.user
      
        nameLabel.text = "Hi \(user?.name ?? "")"
        nameTextField.text = user?.name
        emailTextField.text = user?.email
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func saveButtonTapped(_ sender: Any) {
        
    }
    @IBAction func signOutTapped(_ sender: Any) {
        UserDefaults.standard.deleteToken()
        UserDefaults.standard.deleteUser()
        goToAuth()
    }
    
    @IBAction func cameraButtonTapped(_ sender: Any) {
        pickImage()
    }
    
    func pickImage(){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.showImagePicker(source: .camera)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default,handler: { _ in
            self.showImagePicker(source: .photoLibrary)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            present(actionSheet, animated: true)
    }//:pickImage
    
    func showImagePicker(source: UIImagePickerController.SourceType){
        let viewController = UIImagePickerController()
        viewController.sourceType = source
        viewController.delegate = self
        present(viewController, animated: true)
        
        
    }
    
    @IBAction func addressButtonTapped(_ sender: Any) {
        showAddressViewController { _, address in
            self.addressTextField.text = address
        }
    }
}


extension ProfileViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[.originalImage] as? UIImage
        profileImage = image
        dismiss(animated: true)
    }
}
