//
//  SplashViewController.swift
//  MealMonkey
//
//  Created by Macbook Pro on 30/03/23.
//

import UIKit

class SplashViewController: UIViewController {
    
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let isLogedIn = UserDefaults.standard.accessToken != nil
        DispatchQueue.main.asyncAfter(deadline: .now() + 3){
            
            if isLogedIn{
                self.goToMain()
            }else{
                self.goToAuth()
            }
        }
    }
    
    
}



extension UIWindow{
    func setRootViewController(_ viewController: UIViewController){
        self.rootViewController = viewController
        let option: UIView.AnimationOptions = .transitionCrossDissolve
        let duration: TimeInterval = 0.3
        UIView.transition(with: self, duration: duration,options: option, animations: { } ,completion: { completed in
        })
    }
}

extension UIApplication{
    var window: UIWindow{
        
        if #available(iOS 13.0, *){
            let scenes = UIApplication.shared.connectedScenes
            let windowsScene  = scenes.first as! UIWindowScene
            return windowsScene.windows.first!
            
        }else{
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            return appDelegate.window!
        }
    }
    
}

extension UIViewController{
    func goToMain(){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "Main")
        let window : UIWindow = UIApplication.shared.window
        
        
        window.setRootViewController(viewController)
    }
    
    func goToAuth(){
        let storyBoard = UIStoryboard(name: "Login", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AuthViewController")
        let window : UIWindow = UIApplication.shared.window
        window.setRootViewController(viewController)
        
    }
    
}
