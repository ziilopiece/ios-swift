//
//  RestoViewCell.swift
//  MealMonkey
//
//  Created by Macbook Pro on 04/04/23.
//

import UIKit

class RestoViewCell: UICollectionViewCell {
    
    @IBOutlet weak var foodCategoryLabel: UILabel!
    @IBOutlet weak var restoCategoryLabel: UILabel!
    @IBOutlet weak var ratingAvgLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
}
