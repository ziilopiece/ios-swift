//
//  UserDefaultExtensions.swift
//  MealMonkey
//
//  Created by Macbook Pro on 03/04/23.
//

import Foundation



extension UserDefaults{
    func saveAccessToken(_ accessToken: AccessToken){
        
        let data = try? JSONEncoder().encode(accessToken)
        set(data, forKey: "kAccessTokenKey")
        
        
    }
    
    var accessToken: AccessToken?{
        if let data = self.object(forKey: "kAccessTokenKey") as? Data{
            let accesToken = try? JSONDecoder().decode(AccessToken.self, from: data)
            return accesToken
        }else{
            return nil
        }
    }
    
    func saveUser(_ user: Users){
        let data = try? JSONEncoder().encode(user)
        set(data, forKey: "kUserKey")
    }
    
    var user: Users?{
        if let data = self.object(forKey: "kUserKey") as? Data {
            let user = try? JSONDecoder().decode(Users.self, from: data)
         
            return user
        }else{
            return nil
        }
    }
    
    
    func deleteToken(){
        set(nil, forKey: "kAccessTokenKey")
    }
    func deleteUser(){
        set(nil, forKey: "kUserKey")
    }
    
}
