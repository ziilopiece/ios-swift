//
//  Settings.swift
//  Latihan7-Prototyping
//
//  Created by Macbook Pro on 30/03/23.
//

import SwiftUI

struct Settings: View {
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            
            VStack(spacing: 20) {
                
                // MARK: - SECTION INFO
                sectionInfo
                
                // MARK: - SECTION APPLICATION
                GroupBox {
                    Divider().padding(.vertical, 4)
                    VStack(spacing: 8 ) {
                    SettingsRow(name: "Developer", content: "Zizi Nabila")
                    SettingsRow(name: "Designer", content: "Barou")
                    SettingsRow(name: "Compatibility", content: "iOS 16")
                    SettingsRow(name: "Website", linkLabel: "SahmIlm", linkDestination: "sahmilm.blogspot.com")
                    SettingsRow(name: "Instagram", linkLabel: "ziilopiece", linkDestination: "instagram.com/ziilopiece")
                    SettingsRow(name: "Version", content: "1.1.0")
                    }
                } label: {
                    SettingLabelView(labelText: "Test", labelImage: "pin")
                }
                .padding()
            } //: VSTACK
            
        } //: SCROLLVIEW
    }
}

struct Settings_Previews: PreviewProvider {
    static var previews: some View {
        Settings()
    }
}

// MARK: SETTINGS VIEW
struct SettingLabelView: View {
    
    var labelText: String
    var labelImage: String
    
    var body: some View {
        HStack {
            Text(labelText.uppercased())
                .fontWeight(.bold)
            Spacer()
            Image(systemName: labelImage)
        }
    }
}

// MARK: - Extension
extension Settings {
    // 1. Settings Info
    private var sectionInfo: some View {
        GroupBox {
            Divider().padding(.vertical, 4)
            
            HStack(alignment: .center, spacing: 10) {
                Image("logo")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 80, height: 80)
                    .cornerRadius(10)
                
                Text("SwiftUI helps you build great-looking apps across all Apple Platforms with power if Swift Language, and suprisingly little code. You can bring even better experiences to everyone, on any Apple Device using just one set of tools and APIs")
                    .font(.system(size: 12))
            }
            
        } label: {
            SettingLabelView(labelText: "App Info", labelImage: "info.circle")
        }
        .padding()
    }
}

// MARK: - SETTINGS ROW
struct SettingsRow: View {
    
    var name: String
    var content: String?
    var linkLabel: String?
    var linkDestination: String?
    
    var body: some View {
        HStack {
            Text(name)
                .foregroundColor(.gray)
            Spacer()
            if (content != nil) {
                Text(content ?? "N/A")
            } else if (linkLabel != nil && linkDestination != nil){
                Link(linkLabel!, destination: URL(string: "https://\(String(describing: linkDestination!))")!)
                    .tint(.purple)
                Image(systemName: "arrow.up.right.square")
                    .foregroundColor(.purple)
            } else {
                EmptyView()
            }
        }
    }
}
