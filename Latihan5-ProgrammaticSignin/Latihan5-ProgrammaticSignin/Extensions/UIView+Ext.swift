//
//  UIView+Ext.swift
//  Latihan5-ProgrammaticSignin
//
//  Created by Macbook Pro on 27/03/23.
//

import Foundation
import UIKit

extension UIView {
    func adddOverlay(color: UIColor = .black, alpha: CGFloat = 0.6) {
        let overlay = UIView()
        overlay.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        overlay.frame = bounds
        overlay.backgroundColor = color
        overlay.alpha = alpha
        addSubview(overlay)
    }
}
