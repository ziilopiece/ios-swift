//
//  LoginViewController.swift
//  Latihan5-ProgrammaticSignin
//
//  Created by Macbook Pro on 27/03/23.
//

import UIKit

class LoginViewController: UIViewController {

    weak var bgImaheView: UIImageView!
    weak var titleLabel: UILabel!
    weak var signInButton: PrimaryButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .purple
        
        setupUI()
    }
    
    func setupUI() {
        // MARK: - Image View
        
        let imageView = UIImageView(image: UIImage(named: "bg_sign_in"))
        view.addSubview(imageView)
        self.bgImaheView = imageView
        imageView.contentMode = .scaleAspectFill
        imageView.adddOverlay()
        
        // Constraint dari ImageView
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            imageView.topAnchor.constraint(equalTo: view.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        // MARK: - Title Label
        let titleLabel = UILabel(frame: .zero)
        view.addSubview(titleLabel)
        self.titleLabel = titleLabel
        titleLabel.text = "SIGN IN"
        titleLabel.textColor = .white
        titleLabel.font = UIFont.systemFont(ofSize: 36, weight: .bold)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 60)
        ])
        
        // MARK: - SIGN IN BUTTON
        
        let signInButton = PrimaryButton()
        view.addSubview(signInButton)
        self.signInButton = signInButton
        signInButton.setTitle("SIGN IN", for: .normal)
        signInButton.alpha = 0.6
        
        signInButton.translatesAutoresizingMaskIntoConstraints  = false
        NSLayoutConstraint.activate([
            signInButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            signInButton.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            signInButton.widthAnchor.constraint(equalToConstant: 150),
            signInButton.heightAnchor.constraint(equalToConstant: 46)
        ])
    }


}

