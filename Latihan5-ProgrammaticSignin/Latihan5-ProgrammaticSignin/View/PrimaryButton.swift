//
//  PrimaryButton.swift
//  Latihan5-ProgrammaticSignin
//
//  Created by Macbook Pro on 27/03/23.
//

import UIKit

class PrimaryButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(){
        self.tintColor = .neutral3
        self.backgroundColor = .brand1
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
    }
    
    // ini kita deklarasikan, kerena ingin membuat new button dari super class UIButton
    convenience init() {
        self.init(type: .system)
        setup()
    }

}
