//
//  ListRowView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct ListRowView: View {
    let item: ItemModal
    
    var body: some View {
        HStack {
            VStack {
                if(item.title != ""){
                    Text(item.title).font(.title2).padding(5.0)
                }
                Text(item.text).font(/*@START_MENU_TOKEN@*/.body/*@END_MENU_TOKEN@*/)
                
                Text("Artinya: ").font(.body).padding()
                
                Text(item.translate).font(.body)
                
            }.frame(width: 300.0, alignment: .center)
                .foregroundColor(.white)
            .padding()
        }.background(Color("darkblue"))
        .clipShape(RoundedRectangle(cornerRadius: 10))
        .padding(.vertical, 8)
        .shadow(color: Color("darkblue"), radius: 5, x: 0, y: 5)
        
    }
}

struct ListRowView_Previews: PreviewProvider {
    static var item1 = ItemModal(title: "", text: "Ya Allah, jika aku membiarkan diriku, maka ampunilah dia, dan jika aku menyelamatkannya maka peliharalah dia dengan kekuatan-Mu yang menjaga hamba-hamba-Mu yang saleh", translate: "if you take my soul forgive me ")
    static var item2 = ItemModal(title: "", text: "Wahai Allah, jika Engkau meninggalkan diriku pada diriku sendiri, maka ampunilah diriku. Dan jika Engkau menguji diriku, maka peliharalah dia dengan perlindungan-Mu yang telah Engkau berikan kepada hamba-hamba-Mu yang saleh", translate: "if you take my soul forgive me ")
    static var previews: some View {
        Group{
            ListRowView(item: item1)
            ListRowView(item: item2)
        }
        .previewLayout(.sizeThatFits)
    }
}
