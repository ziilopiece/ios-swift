//
//  RiseAndPrayApp.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

@main
struct RiseAndPray: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
