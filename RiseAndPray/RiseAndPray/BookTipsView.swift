//
//  BookTipsView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct BookTipsView: View {
    var body: some View {
        
        ZStack {
            Image("background").resizable().imageScale(/*@START_MENU_TOKEN@*/.medium/*@END_MENU_TOKEN@*/)
            ScrollView {
                
                Text("Dengan belajar lebih banyak, Anda akan membuat pilihan yang lebih mudah untuk diri sendiri. Anda akan memiliki pemahaman dan tidak perlu bertanya-tanya atau merasa ragu. Klik buku yang ingin Anda lihat lebih detail.").padding([.top, .leading, .trailing])
                
                
                    
                HStack {
                    BokTipsDisplayView(name: "The Prophet Muhammad SAW", link: "https://www.suara.com/news/2022/10/08/073836/kisah-nabi-muhammad-lengkap-sejak-lahir-hingga-wafatnya" )
                    BokTipsDisplayView(name: "The Noble Quran", link: "https://quran.com/")
                }
                        
                        
                        
                HStack {
                    BokTipsDisplayView(name: "The Story of the Qur'an.", link: "https://www.perlego.com/book/1003853/the-story-of-the-quran-its-history-and-place-in-muslim-life-pdf?utm_source=google&utm_medium=cpc&gclid=Cj0KCQjw_8mHBhClARIsABfFgphiG_wfq8vsBT0G1V78q0hLxr9jFuo")
                    BokTipsDisplayView(name: "Islam: A Short History", link: "https://www.suara.com/news/2022/10/08/073836/kisah-nabi-muhammad-lengkap-sejak-lahir-hingga-wafatnya")
                }
                   
                Spacer()
            }.navigationTitle("Book tips")
            .navigationBarItems(leading:
                                    Image("open-book1").resizable().frame(width: 50, height: 50)
            ).foregroundColor(.white)
            
        }
    }
}

struct BookTipsView_Previews: PreviewProvider {
    static var previews: some View {
        BookTipsView()
    }
}

struct BokTipsDisplayView: View {
    var name: String
    var link: String
    var body: some View {
        VStack{
            NavigationLink(
                destination: Webview(url: link),
                label: {
                    ZStack {
                        //Image("openbook")
                        Text(name)
                            .foregroundColor(Color("darkblue"))
                            .font(.caption)
                            .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                            .frame(width: 100, height: 100, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .background(Image("thebook")
                                                .resizable().frame(width: 150, height: 120))
                    }.padding(2.0)
                })
        }.padding(30.0).background(Color(.clear))
    }
}

