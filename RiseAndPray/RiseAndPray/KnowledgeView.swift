//
//  KnowledgeView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct KnowledgeView: View {
    var body: some View {
        ZStack {
            Image("background").resizable().imageScale(.medium)
            ScrollView {
                
                Text("Siapa pun yang masuk Islam harus mengucapkan kata-kata berikut: أشهد أن لا إله إلاَّ الله وأشهد أن محمدًا رسولُ الله 'Tidak ada tuhan selain Tuhan dan Muhammad adalah nabinya'. Syahadat menyiratkan kewajiban untuk menaati Allah dan mengikuti nabi. Untuk melengkapi ini, diperlukan pengetahuan.").padding([.top, .leading, .trailing])
                
                
                
            }.navigationTitle("Glad to Hear")
            .navigationBarItems(leading:
                                    Image("knowledge").resizable().frame(width: 50, height: 50))
            .foregroundColor(.white)
            
        }
    }
}

struct KnowledgeDisplayView: View {
    var title : String
    var text : String
    var body: some View {
        VStack{
            VStack {
                Text(title).font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/).padding()
                Text(text).padding([.leading, .bottom, .trailing])
            }
            .frame(width: 340)
            .background(Color("bluee"))
            .cornerRadius(13.0)
            .foregroundColor(.white)
            
        }
        
    }
}

struct KnowledgeView_Previews: PreviewProvider {
    static var previews: some View {
        KnowledgeView()
    }
}

