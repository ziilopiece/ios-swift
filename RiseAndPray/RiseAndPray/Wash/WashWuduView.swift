//
//  WashWuduView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct WashWuduView: View {
    var body: some View {
        
            ZStack {
                Image("background").resizable().imageScale(.medium).frame(minWidth: 0,
                                   maxWidth: .infinity,
                                   alignment: .topLeading)
                ScrollView {
                    WashTextView(text: "Niyyah, wujudkan niat untuk mensucikan diri di dalam hati. Pergilah ke area pribadi untuk melakukan ritual, jika bisa. Temukan sumber air bersih yang bisa kamu gunakan.")
                    WashTextView(text: "Ucapkan \"bismillah\" dengan lantang untuk menyebut nama Allah.")
                    WashTextView(text: "Cuci tangan hingga dan termasuk pergelangan tangan sebanyak tiga kali")
                    WashTextView(text: "Gunakan tangan kanan Anda untuk mengambil air dan bilas mulut Anda tiga kali dengannya. Bilas air secara menyeluruh untuk membersihkan mulut Anda dan keluarkan.")
                    WashTextView(text: "Bilas hidung Anda tiga kali. Gunakan tangan kanan Anda, cangkir air dan hirup atau hirup melalui hidung Anda tanpa memasukkan terlalu banyak dan tersedak. Gunakan tangan kiri Anda untuk mengeluarkan air dan ulangi proses ini tiga kali. ")
                    WashTextView(text: "Basuh seluruh wajah Anda dengan air. Genggam air di tangan Anda dan basuhlah wajah Anda mulai dari dahi, turunkan air ke garis rahang dan berakhir di dagu.")
                    WashTextView(text: "Cuci lengan Anda. Mulailah dengan mencuci lengan kanan Anda terlebih dahulu, dari ujung jari hingga tepat di atas siku. Seluruh lengan harus basah, tidak ada bagian yang kering. Ulangi tiga kali.")
                    WashTextView(text: "Bersihkan kepala. Tindakan ini dikenal sebagai \"Masah\" untuk membersihkan kepala secara ritual dengan sedikit air. Anda ingin membasahi tangan dan menghilangkan kelebihan air. Lalu usap tangan melalui rambut ke depan dan ke belakang, yaitu dari dahi ke belakang kepala, belakang ke dahi.")
                    
                    WashTextView(text: "Cuci kaki Anda. Mulailah mencuci kaki kanan Anda, mulai dari ujung jari kaki hingga tepat di atas pergelangan kaki. Pastikan untuk mencuci seluruh kaki dan membersihkan sela-sela jari kaki untuk menghilangkan kotoran yang mungkin menumpuk . Ulangi ini tiga kali dan lakukan hal yang sama untuk kaki kiri Anda.")
                    VStack {
                        VStack {
                            Text("LANGKAH-LANGKAH WUDU:").padding()
                            Text("Ini ringkasan langkah-langkahnya. Langkah-langkah ini harus diingat dan dilakukan dalam urutan ini tanpa jeda panjang di antara setiap langkah.").padding().font(.caption)
                            
                        }
                        
                        Text("1 Mulailah dengan niyyah (niat) yang benar, ucapkan Bismillah.").padding().font(.caption)
                        Text("2 Cuci tangan tiga kali, dimulai dengan tangan kanan.").padding().font(.caption)
                        Text("3 Cuci mulutmu tiga kali.").padding().font(.caption)
                        Text("4 Bilas hidung Anda tiga kali.").padding().font(.caption)
                        Text("5 Cuci muka tiga kali.").padding().font(.caption)
                        Text("6 Basuh lengan tiga kali, dimulai dengan lengan kanan dari ujung jari sampai tepat di atas siku.").padding().font(.caption)
                        Text("7 Lap kepala sekali dan bersihkan telinga sekali.").padding().font(.caption)
                        Text("8 Basuh kaki tiga kali, dimulai dengan kaki kanan dari ujung kuku sampai tepat di atas mata kaki.").padding().font(.caption)
                        Image("wash")
                    }
                    
                }.navigationTitle("Wudu")
                .foregroundColor(.white)
                .frame(alignment: .leading)
                
            }
    }
}

struct WashWuduView_Previews: PreviewProvider {
    static var previews: some View {
        WashWuduView()
    }
}
