//
//  WashView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct WashView: View {
    var body: some View {
            ZStack {
                
                Image("background").resizable().imageScale(.medium).frame(minWidth: 0,
                                   maxWidth: .infinity,
                                   alignment: .topLeading)
                VStack {
                    
                    HStack {
                        NavigationLink(
                            destination: WashGhuslView(),
                            label: { GoWashView(text: "Mandi Junub") }
                        )
                        
                        NavigationLink(
                            destination: WashWuduView(),
                            label: { GoWashView(text: "Wudhu") }
                        )
                    }.padding()
                    
                    Text("Mandi junub adalah mandi wajib yang dilakukan oleh umat Islam setelah melakukan hubungan suami istri, atau mimpi basah, atau setelah keluar mani secara tidak sengaja. Mandi junub adalah bagian dari ritual kebersihan dalam agama Islam dan bertujuan untuk membersihkan diri dari najis.").frame(width: 310).padding()
                    
                    Text("Wudhu adalah tata cara bersuci atau ritual cuci wajah, tangan, lengan, kepala, dan kaki yang dilakukan oleh umat Muslim sebelum melaksanakan ibadah seperti shalat. Hal ini dilakukan untuk membersihkan diri dari hadas kecil, yaitu kotoran-kotoran yang keluar dari tubuh seperti kencing, buang air besar, atau keluarnya air mani. Wudhu juga dapat dilakukan sebagai bentuk persiapan mental dan spiritual sebelum melaksanakan ibadah.").frame(width: 320)

                    Spacer()
                }.padding(10)
                .foregroundColor(.white)

            }.navigationTitle("Mandi Junub")
    }
}
struct GoWashView: View {
    var text: String
    var body: some View {
        Text(text)
            .padding()
            .frame(width: 150)
            .background(Color("lightblue"))
            .foregroundColor(.white)
            .font(.title3)
            .cornerRadius(13.0)
            
    }
}





struct WashStepsView: View {
    var name: String
    var text: String
    var body: some View {
        ZStack {
            Image("background").resizable()
            VStack {
                Text(text)
            }
        }
    }
}



struct WashView_Previews: PreviewProvider {
    static var previews: some View {
        WashView()
    }
}

