//
//  WashGhuslView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct WashGhuslView: View {
    var body: some View {
        ZStack {
            Image("background").resizable().imageScale(.medium).frame(minWidth: 0,
                               maxWidth: .infinity,
                               alignment: .topLeading)
            ScrollView {
                WashTextView(text: "Niat, bentuklah niat untuk menyucikan diri di dalam hati. Pergi ke area pribadi untuk melakukan ritual, jika memungkinkan. Temukan sumber air bersih yang dapat Anda gunakan")
                WashTextView(text: "Ucapkan \"bismillah\" dengan keras untuk menyebut nama Allah.")
                WashTextView(text: " Lanjutkan untuk mencuci bagian pribadi Anda dan area yang terkontaminasi.")
                WashTextView(text: "Cuci kepala, muka, dan leher dengan air 3 kali.")
                WashTextView(text: "Basuh seluruh sisi kanan tubuh Anda selanjutnya, dari bahu hingga kaki.")
                WashTextView(text: "Ulangi proses ini di sisi kiri badan.")
                WashTextView(text: "Keringkan dengan handuk bersih, jika diinginkan, dan berpakaianlah.")
                
                Text( "Pertimbangkan juga untuk melakukan mandi junub dalam situasi yang hanya disukai. Ada beberapa situasi di mana ghusl tidak diwajibkan secara ketat, tetapi umumnya dianjurkan. Beberapa keadaan yang dianjurkan untuk mandi adalah: 1 Ketika seorang non-Muslim telah masuk Islam. 2 Sebelum memulai shalat Jumat. 3 Sebelum shalat Ied dimulai. 4 Setelah memandikan jenazah. 5 Sebelum menunaikan ibadah haji ke Mekkah.").font(.caption).padding()
                
                Image("wash")
                
            }.navigationTitle("Mandi Junub")
            .foregroundColor(.white)
            .frame(alignment: .leading)
            
        }
    }
}

struct WashTextView: View {
    var text: String
    var body: some View {
        Text(text)
            .frame(width: 340, alignment: .leading)
            .multilineTextAlignment(.leading)
            .padding()
            .foregroundColor(.white)
            .font(.body)
            
    }
}

struct WashGhuslView_Previews: PreviewProvider {
    static var previews: some View {
        WashGhuslView()
    }
}
