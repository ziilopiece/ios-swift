//
//  DuaOutsideView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct DuaOutsideView: View {
    @State var items: [ItemModal] = [
        ItemModal(title: "Do'a keluar rumah", text: "Bismillahi tawakkaltu 'alallahi wa la haula wa la quwwata illa billahil 'aliyyil 'adhim.", translate: "Dengan nama Allah, aku bertawakal kepada Allah. Tidak ada daya dan kekuatan kecuali dengan (pertolongan) Allah"),
        ItemModal(title: "Doa' di tempat yang Ramai", text: "Laa ilaaha illalloohu wahdahu laa syariika lahu, lahulmulku walahulhamdu, yuhyii wa yumiitu wa huwa hayyun laa yamuutu, biyadihil khoiir, wa huwa ‘ala kulli syai’in qodiir.", translate: "Allah mencatat untuknya satu juta kebaikan, menghapus darinya satu juta keburukan dan meninggikan untuknya satu juta derajat.")
    ]
    
    var body: some View {
        ScrollView {
            ForEach(items){ item in
                ListRowView(item: item)
            }.background(Color.clear)
        }
        
    }
}

struct DuaOutsideView_Previews: PreviewProvider {
    static var previews: some View {
        DuaOutsideView()
    }
}
