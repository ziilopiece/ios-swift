//
//  SickView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct SickView: View {
    @State var items: [ItemModal] = [
        ItemModal(title: "Do'a Menjenguk Orang Sakit", text: "اللهم ربّ الناس، مذهب البأس، اشف أنت الشافي، لا شفاء إلا شفاؤك، شفاءً لا يغادر سقماً", translate: "Ya Allah, semoga Engkau memberikan kesembuhan kepada [sebutkan nama orang sakit] dan mengangkat penyakitnya. Ya Allah, semoga Engkau memberikan kekuatan dan kesehatan kepadanya, dan memperbaiki keadaannya. Ya Allah, semoga Engkau melindungi dan merahmatinya, dan memberikan keselamatan kepada seluruh keluarganya. Amin.")
    ]
    
    var body: some View {
        ScrollView {
            ForEach(items){ item in
                ListRowView(item: item)
            }.background(Color.clear)
        }
        
    }
}

struct SickView_Previews: PreviewProvider {
    static var previews: some View {
        SickView()
    }
}
