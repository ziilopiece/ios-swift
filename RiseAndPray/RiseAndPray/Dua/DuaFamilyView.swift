//
//  DuaFamilyView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct DuaFamilyView: View {
    @State var items: [ItemModal] = [
        ItemModal(title: "Do'a Anak Sholeh", text: "Rabbana hablana, min azwajina wa, dhuriyyatina qurrata, a'yunin, waj'alna, lil muttaqina imama", translate: "Wahai Tuhan kami, anugerahkanlah kepada kami istri-istri dan keturunan kami sebagai penyenang hati, dan jadikanlah kami imam (pemimpin) bagi orang-orang yang bertakwa.")
    ]
    
    var body: some View {
        ScrollView {
            ForEach(items){ item in
                ListRowView(item: item)
            }.background(Color.clear)
        }
        
    }
}

struct DuaFamilyView_Previews: PreviewProvider {
    static var previews: some View {
        DuaFamilyView()
    }
}
