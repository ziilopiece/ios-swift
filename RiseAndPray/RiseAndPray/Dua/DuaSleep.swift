//
//  DuaSleep.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct DuaSleep: View {
    @State var items: [ItemModal] = [
        ItemModal(title: "Do'a berlindung dari Godaan Setan", text: "Bismika rabbi wada'tu janbi, wa bika arfa'hu. In nafsi faghfir laha, wa in arsaltaha fahfaz tahfaz bihi 'ibadaka-s-salihin", translate: "Aku berlindung kepada Allah dari setan yang terkutuk, dan aku berlindung kepada Allah dari kejahatan diriku sendiri dan dari godaan syaitan saat aku mati.")
    ]
    
    var body: some View {
        ForEach(items){ item in
            ListRowView(item: item)
        }.background(Color.clear)
        
    }
}

struct DuaSleep_Previews: PreviewProvider {
    static var previews: some View {
        DuaSleep()
    }
}
