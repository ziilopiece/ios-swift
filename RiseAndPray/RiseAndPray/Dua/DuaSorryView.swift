//
//  DuaSorryView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct DuaSorryView: View {
    @State var items: [ItemModal] = [
        ItemModal(title: "Allahumma Inni As'aluka Al Afiyah", text: "Allahumma inni as'alukal 'afiah, athhibil bass, washfihi, la shifa'a illa shifaun. shifa alla yughadiru saqama", translate: "Ya Allah, aku memohon keselamatan di dunia dan akhirat. Ya Allah aku memohon kebajikan dan keselamatan dalam agama, dunia, keluarga, dan hartaku. Ya Allah, tutupilah auratku (aib dan sesuatu yang tidak layak dilihat orang) dan tenteramkanlah aku dari rasa takut"),
        ItemModal(title: "Rabbana Hablana Min Azwajina", text: "Alla humma inni asalukul jannah", translate: "Ya Tuhanku, jadikanlah aku dan anak cucuku orang-orang yang tetap mendirikan shalat. Ya Tuhan Kami, perkenankanlah doaku")
    ]
    
    var body: some View {
        ScrollView {
            ForEach(items){ item in
                ListRowView(item: item)
            }.background(Color.clear)
        }
        
    }
}

struct DuaSorryView_Previews: PreviewProvider {
    static var previews: some View {
        DuaSorryView()
    }
}
