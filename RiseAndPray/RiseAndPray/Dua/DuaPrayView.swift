//
//  DuaPrayView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct DuaPrayView: View {
    @State var items: [ItemModal] = [
        ItemModal(title: "Setelah Sholat", text: "Allahumma a'inni, ala dhikrika, wa shukrika, wa husni 'ibadatika", translate: "Ya Allah, bantulah aku untuk selalu mengingat-Mu, bersyukur kepada-Mu, dan melaksanakan ibadah dengan sebaik-baiknya."),
        ItemModal(title: "Kesehahteraan", text: "- Subhaanaka Allaahumma wa bihamdika, wa tabaarakasmuka", translate: "Maha Mulia Engkau, Ya Allah, dengan segala puji dan syukur pada-Mu. Diberkati nama-Mu yang agung, dan tertinggi kemuliaan-Mu. Tidak ada yang berhak diibadahi selain dari-Mu"),
        ItemModal(title: "Iftitah", text: "Allaahumma baa'id baynee wa bayna khataayaaya kamaa baa'adta baynal-mashriqi walmaghribi, Allaahumma naqqinee min khataayaaya kamaa yunaqqath-thawbul-'abyadhu minad-danasi, Allaahum-maghsilnee min khataayaaya, bith-thalji walmaa'i walbarad.", translate: "Ya Allah, pisahkanlah aku dari dosa-dosaku sebagaimana Engkau memisahkan timur dari barat. Ya Allah, sucikanlah aku dari pelanggaran-pelanggaranku sebagaimana pakaian putih yang disucikan dari noda. Ya Allah, bersihkanlah dosa-dosaku dengan salju, air, dan embun.")
    ]
    
    var body: some View {
        ScrollView {
            ForEach(items){ item in
                ListRowView(item: item)
            }.background(Color.clear)
        }
        
    }
}

struct DuaPrayView_Previews: PreviewProvider {
    static var previews: some View {
        DuaPrayView()
    }
}

