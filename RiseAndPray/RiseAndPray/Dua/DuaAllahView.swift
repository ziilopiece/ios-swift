//
//  DuaAllahView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct DuaAllahView: View {
    @State var items: [ItemModal] = [
        ItemModal(title: "Doa Setelah Sholat Istikharah", text: "Allaahumma 'innee 'astakheeruka bi'ilmika, wa 'astaqdiruka biqudratika, wa 'as'aluka min fadhtikal-'Adheemi, fa'innaka taqdiru wa laa 'aqdiru, wa ta'lamu, wa laa 'a'lamu, wa 'Anta 'Allaamul-Ghuyoobi, Allaahumma 'in kunta ta'lamu 'anna haathal-'amra-(then mention the thing to be decided) Khayrun lee fee deenee wa ma'aashee wa 'aaqibati 'amree, faqdurhu lee wa yassirhu lee thumma baarik lee feehi, wa 'in kunta ta'lamu 'anna haathal-'amra sharrun lee fee deenee wa ma'aashee wa 'aaqibati 'amree, fasrifhu 'annee wasrifnee 'anhu waqdur liyal-khayra haythu kaana thumma 'ardhinee bihi.", translate: "Ya Allah, sesungguhnya aku beristikharah pada-Mu dengan ilmu-Mu, aku memohon kepada-Mu kekuatan dengan kekuatan-Mu, aku meminta kepada-Mu dengan kemuliaan-Mu. Sesungguhnya Engkau yang menakdirkan dan aku tidaklah mampu melakukannya. Engkau yang Maha Tahu, sedangkan aku tidak tahu. Engkaulah yang mengetahui perkara yang gaib. Ya Allah, jika Engkau mengetahui bahwa perkara ini baik bagiku dalam urusanku di dunia dan di akhirat, maka takdirkanlah hal tersebut untukku, mudahkanlah untukku dan berkahilah ia untukku. Ya Allah, jika Engkau mengetahui bahwa perkara tersebut jelek bagi agama, kehidupan, dan akhir urusanku  maka palingkanlah ia dariku, dan palingkanlah aku darinya, dan takdirkanlah yang terbaik untukku apapun keadaannya dan jadikanlah aku ridha dengannya.")
    ]
    
    var body: some View {
        ScrollView {
            ForEach(items){ item in
                ListRowView(item: item)
            }.background(Color.clear)
        }
    }
}

struct DuaAllahView_Previews: PreviewProvider {
    static var previews: some View {
        DuaAllahView()
    }
}
