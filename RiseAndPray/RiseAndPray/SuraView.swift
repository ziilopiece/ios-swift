//
//  SuraView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct SuraView: View {
    var body: some View {
        ZStack {
            Image("background").resizable().imageScale(.medium).frame(minWidth: 0,
                               maxWidth: .infinity,
                               alignment: .topLeading)
            ScrollView {
                Text("Surah adalah bab dalam kitab suci Islam, Al-Quran. Terdapat 114 surah, yang berbeda panjangnya dari beberapa halaman hingga beberapa kata, yang mencakup satu atau beberapa wahyu yang diterima oleh Nabi Muhammad dari Allah. Berikut adalah beberapa surah yang lebih pendek untuk memulai:").padding()
                VStack{
                    SuraDisplayView(title: "al-fatihah", text: "Qur'an:1")
                    SuraDisplayView(title: "al-Iklas", text: "Qur'an:112")
                    SuraDisplayView(title: "al-falaq", text: "Qur'an:113")
                    SuraDisplayView(title: "an-nas", text: "Qur'an:114")
                    
                    SuraDisplayView(title: "ayat kursi", text: "Qur'an:2:255")
                    
                    SuraDisplayView(title: "?", text: "Qur'an:V:NR")
                    SuraDisplayView(title: "?", text: "Qur'an:V:NR")
                }.frame(maxWidth: .infinity)
            }.foregroundColor(.white).frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
        }.navigationTitle("Surah")
    }
}

struct SuraDisplayView: View {
    var title: String
    var text: String
    var body: some View {
        VStack {
            Text(title).font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/).frame(width: 280,alignment: .leading)
            Text(text).frame(width: 280, alignment: .leading)
        }.frame(width: 300, alignment: .leading).padding().background(Color("darkblue")).cornerRadius(13.0)
    }
    
}

struct SuraView_Previews: PreviewProvider {
    static var previews: some View {
        SuraView()
    }
}

