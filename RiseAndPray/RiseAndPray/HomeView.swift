//
//  HomeView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//


import SwiftUI

struct HomeView: View {
    
    var body: some View {
    
            VStack {
                VStack {
                    Text("Prayer Guide").fontWeight(.semibold).foregroundColor(Color.white).font(.largeTitle)
                    OptionsView()
                }
                
                Image("bismillah").padding(.top, 55.0)
                
                Text("Begin everything by saying Bismillah")
                    .foregroundColor(.white)
                    .padding(.top)
                    .font(.title2)
                
                Spacer()
                
            }.padding(.top, 40.0).padding(.bottom, 15.0)
            .frame(minWidth: 0,
                    maxWidth: .infinity,
                    alignment: .center)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
