//
//  DuaView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct DuaView: View {
    
 
    var body: some View {
    

            VStack {
                Text("DOA").fontWeight(.semibold).foregroundColor(Color.white).font(.largeTitle).padding(.top, 35.0)
                
                ScrollView {
                    Text("Dua adalah sebuah kata Arab yang secara harfiah berarti keinginan, permohonan, doa, atau ajakan. Dalam ayat Al-Quran 40:60, Allah memerintahkan kita untuk memohon kepada-Nya. Dalam sebuah hadis, Dua dijelaskan sebagai sebuah sarana untuk mendekatkan diri kepada Allah dalam ibadah.").padding().foregroundColor(.white)
                    
                    NavigationLink(
                        destination: DuaSleep(),
                        label: {
                            DuaButtonView(text: " Sleep",img: "sleeping")
                        })
                    
                    
                    NavigationLink(
                        destination: DuaOutsideView(),
                        label: {
                            DuaButtonView(text: "Outdoor",img: "exit-door")
                        })
                    
                    NavigationLink(
                        destination: DuaPrayView(),
                        label: {
                            DuaButtonView(text: "Pray",img: "praying")
                        })

                    
                    NavigationLink(
                        destination: DuaFamilyView(),
                        label: {
                            DuaButtonView(text: "Family",img: "family")
                        })
                    
                    NavigationLink(
                        destination: DuaAllahView(),
                        label: {
                            DuaButtonView(text: "Praise Allah", img: "allah")
                        })
                    NavigationLink(
                        destination: SickView(),
                        label: {
                            DuaButtonView(text: "Illness and Death",img: "fever")
                        }).padding(.bottom, 70.0)
                    
                }
                }
            }
        }

struct DuaButtonView: View {
    var text: String
    var img: String
    var body: some View {
                HStack{
                    Image(img).resizable().frame(width: 50, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    Text(text)
                }.frame(width: 300, alignment: .leading)
                .padding()
                .background(Color("darkblue"))
                .cornerRadius(13).padding(.horizontal, 5.0)
                .shadow(color: Color("darkblue"), radius: 5, x: 0, y: 5)
                .foregroundColor(.white)
        
    }
    
}

struct DuaView_Previews: PreviewProvider {
    static var previews: some View {
        DuaView()
            .previewDevice("iPhone SE (2nd generation)")
    }
}
