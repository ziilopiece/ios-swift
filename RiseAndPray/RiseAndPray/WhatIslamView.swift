//
//  WhatIslamVie.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct WhatIsIslamView: View {
    var items: [WhatIsIslamModel] {
        let results = WhatIsIslamProvider.all()
        return results
    }
    var body: some View {
        ZStack {
            Image("background").resizable().imageScale(.medium)
            ScrollView {
                VStack(alignment: .leading) {
                    ForEach(items) { item in
                        WhatIsIslamDisplayView(text: item.text)
                    }
                }
                .padding()
            }.navigationTitle("Apa itu Islam ?")
                .foregroundColor(.white)
                .font(.caption)
                .navigationBarItems(leading:
                                        Image("islamic-pray").resizable().frame(width: 50, height: 50))
        }
    }
}

struct WhatIsIslamDisplayView: View {
    var text: String
    var body: some View {
        Text(text).padding().font(.system(size: 19))
    }
}

struct WhatIsIslamView_Previews: PreviewProvider {
    static var previews: some View {
        WhatIsIslamView()
    }
}
