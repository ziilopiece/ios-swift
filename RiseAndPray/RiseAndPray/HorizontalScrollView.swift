//
//  HorizontalScrollView.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import SwiftUI

struct HorizontalScrollView: View {
    private var items: [ItemPrayModel] {
        
        let results = ItemPrayProvider.all()
        return results
        
    }
    @State var backgrundOffset : CGFloat = 0
    var body: some View {
        GeometryReader{ g in
            HStack(spacing: 0){
                
                ForEach(items) { item in
                    VStack{
                        VStack {
                            Image(item.img).resizable().frame(width: 140, height: 180, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        }.frame(width: 360, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .padding(.top, 50.0)
                        Text("").frame(height: 50)
                        ScrollView{
                            StepDisplayView(name: item.name, text: item.text, input: item.input, trans: item.trans)
                            Image("").frame(width: 30, height: 100)
                        }.frame(maxHeight: .infinity)
                        .padding()
                        Text("").frame(height: 50)
                    }
                }
                    .frame(width:g.size.width, height: g.size.height)
            }
            .offset(x: -(self.backgrundOffset * g.size.width))
            .animation(.default)
            
            ZStack{
                Rectangle()
                    .fill(Color.clear)
                    .frame(width:g.size.width, height: 40)
                
                HStack{
                    
                    ForEach(0 ..< items.count, id: \.self) {value in
                        Circle()
                            .fill(Color("lightblue"))
                            .frame(width: self.backgrundOffset == CGFloat(value) ? 10 : 5, height: self.backgrundOffset == CGFloat(value) ? 10 : 5)
                            .overlay(
                                Circle()
                                    .stroke(Color.white, lineWidth: 3)
                            ).padding(.trailing, 1.0)
                    }
                    
                }
                .animation(.default)
                
            }.position(x: g.size.width/2, y:g.size.height/2.5)
             
        }
        
        .gesture(
            DragGesture()
                .onEnded({ value in

                    if value.translation.width > 5 {
                        if self.backgrundOffset > 0 {
                        self.backgrundOffset -= 1
                        }

                    }else if value.translation.width < -5 {
                        if self.backgrundOffset < CGFloat(items.count - 1) {
                        self.backgrundOffset += 1
                        }
                    }
                })
        )
    }
}

struct HorizontalScrollView_Previews: PreviewProvider {
    static var previews: some View {
        HorizontalScrollView()
    }
}
