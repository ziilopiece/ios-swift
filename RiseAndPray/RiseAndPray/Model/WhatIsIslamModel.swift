//
//  WhatIsIslamModel.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import Foundation

struct WhatIsIslamModel: Identifiable {
    let id: String = UUID().uuidString
    let text : String
}

public struct WhatIsIslamProvider {
    static func all() -> [WhatIsIslamModel] {
        return [
         
            WhatIsIslamModel(text: "1. Keyakinan kepada Tuhan yang Esa: Islam mengajarkan bahwa hanya ada satu Tuhan yang menciptakan alam semesta dan segala isinya. Tuhan ini tidak memiliki bentuk fisik, tidak memiliki anak atau pasangan, dan tidak dapat digambarkan dengan kata-kata manusia. Seluruh umat manusia diminta untuk menyembah dan mengikuti perintah-Nya."),
            WhatIsIslamModel(text: "2. Menekanan pada keadilan sosial: Islam mengajarkan pentingnya keadilan sosial dalam segala aspek kehidupan, baik itu dalam hubungan antar individu, antara kelompok, maupun antara individu dengan negara. Dalam Islam, orang kaya diminta untuk membantu orang miskin, dan semua orang diberikan hak yang sama untuk memperoleh kesempatan dan perlindungan dari negara."),
            WhatIsIslamModel(text: "3. Lima rukun Islam: Islam memiliki lima rukun yang menjadi dasar praktik ibadah, yaitu syahadat, shalat, zakat, puasa, dan haji. Kelima rukun ini merupakan kewajiban bagi setiap muslim yang mampu melaksanakannya."),
            WhatIsIslamModel(text: "4. Kehidupan akhirat: Islam mengajarkan bahwa setiap manusia akan dipertanggungjawabkan atas segala tindakan yang dilakukannya selama hidup di dunia dan akan diadili di akhirat. Kehidupan di akhirat dipercayai sebagai kehidupan yang abadi, di mana manusia akan menerima balasan sesuai dengan amalannya."),
            WhatIsIslamModel(text: "5. Keanekaragaman: Islam mengajarkan pentingnya menghormati perbedaan antara satu sama lain dan menerima keberagaman. Islam mengakui keberadaan ajaran-ajaran agama lain dan meminta umatnya untuk berdialog dengan orang-orang dari berbagai agama dan budaya untuk mencapai perdamaian dan persatuan di dunia.")
        
        ]
    }
}
