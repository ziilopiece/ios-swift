//
//  ItemModal.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import Foundation

struct ItemModal: Identifiable {
    let id: String = UUID().uuidString
    let title: String
    let text: String
    let translate: String
}
