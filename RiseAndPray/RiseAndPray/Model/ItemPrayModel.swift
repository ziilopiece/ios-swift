//
//  ItemPray.swift
//  RiseAndPray
//
//  Created by Macbook Pro on 08/04/23.
//

import Foundation

struct ItemPrayModel: Identifiable {
    let id: String = UUID().uuidString
    var name : String
    var text : String
    var input: String
    var trans : String
    var img : String
}

public struct ItemPrayProvider{
    static func all() -> [ItemPrayModel] {
        return [
            
            ItemPrayModel(name: "Takbiratul Ikhram", text: "\"ALLAHU AKBAR\"", input: "\"Melakukan takbiratul ihram dengan mengangkat kedua tangan sejajar dengan telinga (untuk laki-laki) atau sejajar dengan dada (untuk perempuan) sembari mengucapkan 'Allaahu akbar' yang artinya 'Allah Maha Besar'.\"", trans: "Allah Maha Besar", img: "takbeer"),
            ItemPrayModel(name: "Membaca Al-fatihah", text: "\"Bismillahi Rahmani Rahime, Alhamdou illahi rabi l’alamine, A rahmani rahime, Maliki yawmiddine, iyaka na’abodo wa iyaka nasta’ine, Indina siratal mostakime siratal ladina, Anamma ’alayhime, Rayi l’mardobi ’aleyhime wa la da line. Amine.\"", input: "Dilanjutkan dengan membaca surat Al-Fatihah.", trans: "Dengan menyebut nama Allah Yang Maha Pemurah lagi Maha Penyayang. Segala puji bagi Allah, Tuhan semesta alam. Maha Pemurah lagi Maha Penyayang. Yang menguasai di Hari Pembalasan. Hanya kepada Engkaulah kami menyembah dan hanya kepada Engkaulah kami meminta pertolongan. Tunjukilah kami jalan yang lurus. (Yaitu) Jalan orang-orang yang telah Engkau beri nikmat kepada mereka; bukan (jalan) mereka yang dimurkai dan bukan (pula jalan) mereka yang sesat.\"",  img: "qiyyam"),
            ItemPrayModel(name: "Membaca Surah Pendek", text: "\"Qoul houwa’llahou ahad, Allahou samade, Lame valid wa lame youlad, wa lame yacoune lahou koufouane ahad\"", input: "Surat pendek dibaca setelah selesai membaca surat Al-Fatihah. Umat Islam dapat memilih surat pendek apa saja, sesuai dengan yang dihapalkan. Biasanya surat pendek dibaca pada dua rakaat pertama dalam sholat.", trans: "String",  img: "qiyyam"),
            ItemPrayModel(name: "Rukuk", text: " Subhanna rabbil-al-athemi, subhaana rabbi-al-athemi, subhaana rabbi-al-athem ", input: "ukuk adalah posisi dalam sholat yang dilakukan setelah membaca ayat atau surat pendek. Gerakan rukuk dilakukan diawali dengan mengangkat kedua tangan dan membaca 'Allahu akbar'. Selanjutnya, badan dibungkukkan dan kedua tangan memegang lutut. Usahakan antara punggung dan kepala posisinya sama rata.", trans: "Maha suci tuhan yang maha agung serta memujilah aku kepadanya." , img: "roukoue"),
            ItemPrayModel(name: "Itidal", text: "\"Sami allahou liman hamidah rabbana laka-hamdu\"", input: "", trans: "Ya Allah tuhan kami, bagimu segala puji sepenuh langit dan bumi, dan sepenuh sesuatu yang engkau kehendaki sesudah itu.",  img: "qiyyam"),
            ItemPrayModel(name: "Sujud", text: "\"Subhanna rabbi-al-ala, Subhanna rabbi-al-ala, Subhanna rabbi-al-ala \"", input: "Kemudian melakukan sujud dengan cara meletakkan dahi di lantai yang telah diberikan alas bersih atau sajadah. Ketika turun ke bawah dari posisi i'tidal, lakukan sambil membaca 'Allahu akbar' dan selanjutnya dengan membaca doa sujud sebanyak 3 kali", trans: "Maha suci tuhan yang maha tinggi serta memujilah aku kepadanya.",  img: "sujud"),
            ItemPrayModel(name: "Duduk di Antara Dua Sujud", text: "\"Allahu akbar\"", input: "Rabbighfirli warhamni wajiburni warfa'ni warzuqni wahdini wa'afini wa'fuanni", trans: "Ya Allah ampunilah dosaku, belas kasihanilah aku, cukupkanlah segala kekurangan dan angkatlah derajatku, berilah rizki kepadaku, berilah aku petunjuk, berilah kesehatan kepadaku dan berilah ampunan kepadaku",  img: "jalssa"),
            ItemPrayModel(name: "Sujud", text: "\"Subhanna rabbi-al-ala, Subhanna rabbi-al-ala, Subhanna rabbi-al-ala \"", input: "Kemudian melakukan sujud dengan cara meletakkan dahi di lantai yang telah diberikan alas bersih atau sajadah. Ketika turun ke bawah dari posisi i'tidal, lakukan sambil membaca 'Allahu akbar' dan selanjutnya dengan membaca doa sujud sebanyak 3 kali", trans: "Maha suci tuhan yang maha tinggi serta memujilah aku kepadanya.",  img: "sujud"),
            ItemPrayModel(name: "Tasyahud Akhir", text: "\"At-thiyyatu lillahi wa 's-salawatu wa 't-tayyibatu as-salamu 'alayka ayyuha'n-nabiyyu wa rahmatullahi wa barakatuh as-salamu 'alayna wa ala ibadillahi's-saliheen ashadu an la ilaha illa allah wa ashhadu anna muhammadan 'abduhu wa rasuluh. \"", input: "", trans: "Segala penghormatan, keberkahan, dan kebaikan hanyalah milik Allah. Semoga keselamatan, rahmat, dan berkah Allah senantiasa tercurah kepada Nabi Muhammad. Semoga keselamatan juga tercurah kepada kami dan kepada seluruh hamba Allah yang saleh. Aku bersaksi bahwa tiada tuhan selain Allah dan aku bersaksi bahwa Muhammad adalah hamba dan rasul-Nya.",  img: "tachahoud"),
            ItemPrayModel(name: "Salam", text: "\"Aa salam alaykum wa rahmatullahi wa barakatuhu. Aa salam alaykum wa rahmatullahi wa barakatuhu\"", input: "När du säger den meningen kollar du till höger.", trans: "Gud är stor/störst",  img: "jalssa"),
        
        
        ]
    }
}
